<%-- 
    Document   : movieinfo
    Created on : Mar 16, 2020, 3:34:53 PM
    Author     : DVMBM50
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Movie Info</title>
    </head>
    <body>
        <h1>Movie Info</h1>
        <form action="./MoviesServlet" method="POST">
            <table>
                <tr>
                    <td>Movie ID</td>
                    <td><input type="text" name="movieId" value="${movie.id}" /></td>
                </tr>

                <tr>
                    <td>Title</td>
                    <td><input type="text" name="title" value="${movie.title}" /></td>
                </tr>

                <tr>
                    <td>Release Date</td>
                    <td><input type="text" name="releasedate" value="${movie.releasedate}" /></td>
                </tr>

                <tr>
                    <td>Runtime</td>
                    <td><input type="text" name="runtime" value="${movie.runtime}" /></td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="Add"value="Add"/>
                        <input type="submit" name="action" value="Edit"/>

                        <input type="submit" name="action" value="Delete"/>
                        <input type="submit" name="action" value="Search"/>
                    </td>
                </tr>
            </table>
        </form>
        <br>
        <table border="1">
            <th>ID</th>
            <th>title</th>
            <th>release date</th>
            <th>runtime</th>
                <c:forEach items="${allMovies}" var="mov">
                <tr>
                    <td>${mov.movieId}</td>
                    <td>${mov.title}</td>
                    <td>${mov.releasedate}</td>
                    <td>${mov.runtime}</td>
                </tr>
                </c:forEach>
        </table>
    </body>
</html>
