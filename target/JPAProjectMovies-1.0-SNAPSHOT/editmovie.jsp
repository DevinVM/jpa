<%-- 
    Document   : editmovie
    Created on : Mar 9, 2020, 4:47:16 PM
    Author     : DVMBM50
--%>

<%@page import="com.movies.DAO.DAOMovie"%>
<%@page import="com.movies.beans.Movie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <% 
        Movie updatemovie = (Movie) request.getAttribute("updatemovie");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
        <title>Edit Movie</title>
    </head>
    <body>

    <div align="center">
        <h1>Edit Movie Page</h1>
    </div>
        
        <!-- Sidebar/menu -->
  <nav class="w3-sidebar w3-red w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
  <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px">Close Menu</a>
  <div class="w3-container">
    <h3 class="w3-padding-64"><b>Devin<br>Van Malder</b></h3>
  </div>
  <div class="w3-bar-block">
    <a href="MovieServlet?showallmovies" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Show Movies Table</a> 
    <a href="index.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Add Movie</a> 
    <a href="stylesheet.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Links</a>
  </div>
</nav>
        
<form name="editmovie" action="MovieServlet">
    <div align="center"> 
        <input type="hidden" value="<%=updatemovie.getId()%>" name="id" placeholder="Movie ID"/>
        
        Titel:
        <input type="text" value="<%=updatemovie.getTitle()%>" name="title" placeholder="Title" size="20"/><br>
        
        Release Date:
        <input type="text" value="<%=updatemovie.getReleasedate()%>"name="releasedate" placeholder="Release Date" size="20"/><br>
        
        Runtime:
        <input type="text" value="<%=updatemovie.getRuntime()%>" placeholder="Runtime"  name="runtime" size="20"/><br>
        
        <br> 
        <button class="btn btn-primary" type="submit" value="submit" name="editmovie">Edit Movie</button>
    </div>
        
</form>
        
</body>
</html>
