<%-- 
    Document   : newjsp
    Created on : Mar 11, 2020, 2:58:56 PM
    Author     : DVMBM50
--%>
<!DOCTYPE html>
<html lang="en">
    <title>W3.CSS Template</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
        body {font-size:16px;}
        .w3-half img{margin-bottom:-6px;margin-top:16px;opacity:0.8;cursor:pointer}
        .w3-half img:hover{opacity:1}
    </style>
    <body>

        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-red w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px">Close Menu</a>
            <div class="w3-container">
                <h3 class="w3-padding-64"><b>Devin<br>Van Malder</b></h3>
            </div>
            <div class="w3-bar-block">
                <a href="MovieServlet?showallmovies" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Show Movies Table</a> 
                <a href="index.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Add Movie</a> 
                <a href="stylesheet.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Links</a>
            </div>
        </nav>
        <div align ="center">   
            <h1>Movie have been Deleted</h1>
        </div> 




    </body>
</html>
