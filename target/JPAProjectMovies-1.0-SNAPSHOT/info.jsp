<%-- 
    Document   : info
    Created on : Mar 20, 2020, 9:29:01 AM
    Author     : DVMBM50
--%>

<%@page import="java.util.List"%>
<%@page import="com.movies.beans.Movie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>More Info</title>
    </head>
    <body>
        <h1>More info Page</h1>
            <form action="MovieServlet">                     
                    <%
                        List<Movie> movie = (List<Movie>) request.getAttribute("moreinfo");
                        for (Movie mov : movie) {
                    %>      
                    
                    <div align ="center">
        ID:
        <input type="text" value="<%=mov.getId()%>" name="id"/>
                        
        Titel:
        <input type="text" value="<%=mov.getTitle()%>" name="title"/><br>
        
        Release Date:
        <input type="text" value="<%=mov.getReleasedate()%>"name="releasedate"/><br>
        
        Runtime:
        <input type="text" value="<%=mov.getRuntime()%>" name="runtime"/><br>
        <br>
                    </div>
        <%
          }
        %>
            </form>
                    <input type="button" value="submit"><a href="showallmovies.jsp">Show All Movies Again</a></input>

            
            </body>

            
            </html>
