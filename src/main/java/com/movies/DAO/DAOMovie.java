/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.DAO;

import com.movies.beans.Movie;
import com.movies.beans.Movie1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author DVMBM50
 */
public class DAOMovie {
    
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_JPAProjectMovies_war_1.0-SNAPSHOTPU");
    private EntityManager entityManager = emf.createEntityManager();
    private EntityTransaction et = entityManager.getTransaction();
    
 /*           public static Connection getConnection(){  
    Connection con=null;  
    try{  
        Class.forName("com.mysql.jdbc.Driver");  
        con=DriverManager.getConnection("jdbc:mysql://localhost:3306/movietest","root","");  
    }catch(Exception e){System.out.println(e);}  
    return con;  
    }
    public List<Movie> findAllMovies() throws SQLException{
        
        et.begin();
        TypedQuery<Movie> query = em.createNamedQuery("Movie.findAll", Movie.class);
        List<Movie> movies = query.getResultList();
        et.commit();
        
        return movies;
        
    }*/
        public void editmovie(Movie updatemovie) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        Movie movie = entityManager.find(Movie.class, updatemovie.getId());
        movie.setTitle(updatemovie.getTitle());
        movie.setReleasedate(updatemovie.getReleasedate());
        movie.setRuntime(updatemovie.getRuntime());
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    
        public List<Movie> findAllMovies() {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("SELECT m FROM Movie m");
        List<Movie> allMovies = (List<Movie>)  query.getResultList();
        entityManager.getTransaction().commit(); entityManager.close();

        return allMovies;
    }

    

    
     public Movie  getMovie(int id) {
        EntityManager entityManager = emf.createEntityManager();
        Movie movie = entityManager.createNamedQuery("Movie.findById", Movie.class).setParameter("id", id).getSingleResult();
        entityManager.close();
        return movie;
    }

      public void delete(int id) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin(); 
        Movie movie = entityManager.find(Movie.class, id);
        entityManager.remove(movie); entityManager.getTransaction().commit(); entityManager.close();
    }

    
    
    
    

/*
// Doet Niks    
    public static int delete(Movie1 u){  
    int status=0;  
    try{  
        Connection con=getConnection();  
        PreparedStatement ps=con.prepareStatement("delete from movie where id=?");  
        ps.setInt(1,u.getId());  
        status=ps.executeUpdate();  
    }catch(SQLException e){System.out.println(e);}  
  
    return status;  
    }
   
// Doet Niks 
    public static Movie getRecordById(int id){  
    Movie u=null;  
    try{  
        Connection con=getConnection();  
        PreparedStatement ps=con.prepareStatement("select * from movie where id=?");  
        ps.setInt(1,id);  
        ResultSet rs=ps.executeQuery();  
        while(rs.next()){  
            u=new Movie();  
            u.setId(rs.getInt("id"));  
            u.setTitle(rs.getString("title"));  
            u.setReleasedate(rs.getString("releasedate"));  
            u.setRuntime(rs.getString("runtime"));  
              
        }  
    }catch(SQLException e){System.out.println(e);}  
    return u;  
    }
     
// Doet Niks 
    public static int save(Movie u){  
    int status=0;  
    try{  
        Connection con=getConnection();  
        PreparedStatement ps=con.prepareStatement(  
        "insert into movie(title,releasedate,runtime) values(?,?,?)");  
        ps.setString(1,u.getTitle());  
        ps.setString(2,u.getReleasedate());  
        ps.setString(3,u.getRuntime());  
          
        status=ps.executeUpdate();  
    }catch(Exception e){System.out.println(e);}  
    return status;  
    }*/  

    

}


