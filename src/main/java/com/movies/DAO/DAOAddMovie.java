/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.DAO;

import com.movies.beans.Movie;
import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author DVMBM50
 */
public class DAOAddMovie {
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.mycompany_JPAProjectMovies_war_1.0-SNAPSHOTPU");
    private EntityManager em = emf.createEntityManager();
    private EntityTransaction et = em.getTransaction();
    
    public void addMovie(String title , String releasedata , String runtime) throws SQLException {
        
        Movie movie = new Movie();
        movie.setTitle(title);
        movie.setReleasedate(releasedata);
        movie.setRuntime(runtime);
        
        et.begin();
        em.persist(movie);
        et.commit();
        
    }
}
