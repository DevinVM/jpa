/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.controller;

import com.movies.DAO.DAOAddMovie;
import com.movies.DAO.DAOMovie;
import com.movies.beans.Movie;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DVMBM50
 */
@WebServlet(name = "MovieServlet", urlPatterns = {"/MovieServlet"})
public class MovieServlet extends HttpServlet {

    DAOMovie daoMovie;
    DAOAddMovie daoAddMovie;

    @Override
    public void init() throws ServletException {

        daoMovie = new DAOMovie();
        daoAddMovie = new DAOAddMovie();

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        RequestDispatcher requestDisp = showAll(request, response);

        if (request.getParameter("moreinfo") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            requestDisp = showMoreInfo(id, request, response);
            request.setAttribute("movie", id);

        } else if (request.getParameter("delete") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            requestDisp = deleteId(id, request, response);
            requestDisp = request.getRequestDispatcher("newjsp.jsp");

        } else if (request.getParameter("editMovieid") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            Movie movie = daoMovie.getMovie(id);
            request.setAttribute("updatemovie", movie);
            requestDisp = request.getRequestDispatcher("editmovie.jsp");

        } else if (request.getParameter("editmovie") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            editMovieId(id, request, response);
            requestDisp = request.getRequestDispatcher("/WEB-INF/movieEditedSuccess.jsp");

        } else if (request.getParameter("addmovie") != null) {
            addmoviejsp(request, response);
        }

        requestDisp.forward(request, response);
    }

    private RequestDispatcher showMoreInfo(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("info.jsp");
        List<Movie> movie = daoMovie.findAllMovies();
        request.setAttribute("moreinfo", movie);
        return rd;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(MovieServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private RequestDispatcher showAll(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher requestDisp = request.getRequestDispatcher("showallmovies.jsp");
        List<Movie> movie = daoMovie.findAllMovies();
        request.setAttribute("movie", movie);
        return requestDisp;
    }

    private RequestDispatcher editMovieId(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        RequestDispatcher requestDisp = request.getRequestDispatcher("editmovie.jsp");
        String title = request.getParameter("title");
        String releasedate = request.getParameter("releasedate");
        String runtime = request.getParameter("runtime");
        Movie updateMovie = new Movie(id, title, releasedate, runtime);
        daoMovie.editmovie(updateMovie);
        return requestDisp;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(MovieServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /* private void showallmoviesjsp(HttpServletRequest request, HttpServletResponse response) {
        if (request.getParameter("showallmovies") != null) {
            try {
                List<Movie> showmovies = daoMovie.findAllMovies();

                if (showmovies.isEmpty()) {
                    String foutBoodschap = "Er is geen Data Beschikbaar gelieven data in te voeren";
                    request.setAttribute("foutBoodschap", foutBoodschap);
                    request.getRequestDispatcher("/WEB-INF/fout.jsp").forward(request, response);
                } else {
                    request.setAttribute("movies", showmovies);
                    request.getRequestDispatcher("showallmovies.jsp").forward(request, response);
                }

            } catch (ServletException | IOException | SQLException ex) {
                Logger.getLogger(MovieServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
    }*/
    private void addmoviejsp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String titlename = request.getParameter("title");
        String releasedate = request.getParameter("releasedate");
        String runtime = request.getParameter("runtime");

        try {
            daoAddMovie.addMovie(titlename, releasedate, runtime);
            request.getRequestDispatcher("added.jsp").forward(request, response);

        } catch (SQLException ex) {
            Logger.getLogger(MovieServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private RequestDispatcher deleteId(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher requestDisp = request.getRequestDispatcher("showallmovies.jsp");
        daoMovie.delete(id);
        return requestDisp;
    }

}
