/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.controller;

import com.movies.beans.Movie;
import com.movies.beans.MovieDAOLocal;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DVMBM50
 */
@WebServlet(name = "MoviesServlet", urlPatterns = {"/MoviesServlet"})
public class MoviesServlet extends HttpServlet {

    @EJB
    private MovieDAOLocal movieDao;

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String movieIdStr = request.getParameter("movieId");
        
        int movieId = 0;
            if(movieIdStr!=null && !movieIdStr.equals(""))
                movieId=Integer.parseInt(movieIdStr);
        
        String title = request.getParameter("title");
        String releasedate = request.getParameter("releasedate");
        String runtime = request.getParameter("runtime");

        Movie movie = new Movie(movieId, title, releasedate, runtime);

        
        if (request.getParameter("Add") != null) {
            movieDao.addMovie(movie);
        } else if (request.getParameter("Edit") != null) {
            movieDao.editMovie(movie);
        } else if (request.getParameter("delete") != null) {
            movieDao.deleteMovie(movieId);
        } else if (request.getParameter("search") != null) {
            movie = movieDao.getMovie(movieId);
        }
        request.setAttribute("movie", movie);
        request.setAttribute("allMovies", movieDao.getAllMovies());
        request.getRequestDispatcher("movieinfo.jsp").forward(request, response);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
