/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.beans;

/**
 *
 * @author DVMBM50
 */
public class Movie1 {
    private int id;
    private String title;
    private String releasedate;
    private String runtime;

    public Movie1() {
    }

    public Movie1(int id, String title, String releasedate, String runtime) {
        this.id = id;
        this.title = title;
        this.releasedate = releasedate;
        this.runtime = runtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleasedate() {
        return releasedate;
    }

    public void setReleasedate(String releasedate) {
        this.releasedate = releasedate;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }
    
    
}
