/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class MovieDAO implements MovieDAOLocal {
@PersistenceContext
private EntityManager em;

    @Override
    public void editMovie(Movie movie) {
        em.merge(movie);
    }

    @Override
    public Movie getMovie(int movieId) {
        return em.find(Movie.class, movieId);
    }

    @Override
    public void deleteMovie(int movieId) {
        em.remove(getMovie(movieId));
    }

    @Override
    public List<Movie> getAllMovies() {
        return em.createNamedQuery("Movie.findAll").getResultList();
    }

    @Override
    public void addMovie(Movie movie) {
        em.persist(movie);
    }
    
    
    
    
    
}
