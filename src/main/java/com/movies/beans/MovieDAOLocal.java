/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movies.beans;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author DVMBM50
 */
@Local
public interface MovieDAOLocal {

    void editMovie(Movie movie);

    Movie getMovie(int movieId);

    void deleteMovie(int movieId);

    List<Movie> getAllMovies();

    void addMovie(Movie movie);




    
}
