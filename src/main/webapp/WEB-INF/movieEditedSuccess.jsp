<%-- 
    Document   : movieEditedSuccess
    Created on : Mar 19, 2020, 3:12:20 PM
    Author     : DVMBM50
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Success</title>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    </head>
    <body>
        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-red w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px">Close Menu</a>
            <div class="w3-container">
                <h3 class="w3-padding-64"><b>Devin<br>Van Malder</b></h3>
            </div>
            <div class="w3-bar-block">
                <a href="MovieServlet?showallmovies" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Show Movies Table</a> 
                <a href="index.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Add Movie</a> 
                <a href="stylesheet.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Links</a>
            </div>
        </nav>
        <div align ="center">
            <h1>Movie Have Been Editted</h1>
        </div>
    </body>
</html>
