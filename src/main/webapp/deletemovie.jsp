<%-- 
    Document   : showallmovies
    Created on : Mar 9, 2020, 3:06:35 PM
    Author     : DVMBM50
--%>

<%@page import="java.util.List"%>
<%@page import="com.movies.beans.Movie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
        <title>ShowAllMovies</title>
        <style>
            body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
            body {font-size:16px;}
            .w3-half img{margin-bottom:-6px;margin-top:16px;opacity:0.8;cursor:pointer}
            .w3-half img:hover{opacity:1}
        </style>
        <style>
            #mySidebar {
                margin-bottom: 50px !important;
            }
            #details {
                margin-left: 350px;
            }
        </style>

    </head>
    <body>

        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-red w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px">Close Menu</a>
            <div class="w3-container">
                <h3 class="w3-padding-64"><b>Devin<br>Van Malder</b></h3>
            </div>
            <div class="w3-bar-block">
                <a href="MovieServlet?showallmovies" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Show Movies Table</a> 
                <a href="index.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Add Movie</a> 
                <a href="stylesheet.jsp" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Links</a>
            </div>
        </nav>
    </body>
</html>